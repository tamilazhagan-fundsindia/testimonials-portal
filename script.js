var testimonialArray = [];
var testimonial = {};
var totalId = 0;
var editingId = -1;
var modalInstance;

document.addEventListener("DOMContentLoaded", function() {
  customerName = document.getElementById("customer_name");
  forPerson = document.getElementById("for_person");
  content = document.getElementById("content");
  var tooltip = document.querySelectorAll(".tooltipped");
  M.Tooltip.init(tooltip);
  var modalElement = document.querySelectorAll(".modal");
  M.Modal.init(modalElement, {
    startingTop: "35%",
    endingTop: "35%",
    dismissible: false
  });
  modalInstance = M.Modal.getInstance(modalElement);
});

function initializeCollapsible() {
  var elems = document.querySelectorAll(".collapsible");
  M.Collapsible.init(elems);
}

function addTestimonial() {
  if (
    customerName.value != "" &&
    content.value != "" &&
    forPerson.value != ""
  ) {
    testimonial.customerName = customerName.value;
    testimonial.content = content.value;
    testimonial.forPerson = forPerson.value;
    if (editingId > -1) {
      testimonial.id = editingId;
      this.testimonialArray[editingId] = Object.assign({}, testimonial);
      M.toast({ html: "Updated successfully", displayLength: 2400 });
    } else {
      testimonial.id = totalId;
      testimonialArray.push(Object.assign({}, testimonial));
      totalId++;
      M.toast({ html: "Added successfully", displayLength: 2400 });
    }
    showTestimonials();
    clearTestimonial(false);
  } else {
    M.toast({ html: "Kindly enter all the fields", displayLength: 2400 });
  }
}

function clearTestimonial(show) {
  customerName.value = "";
  content.value = "";
  forPerson.value = "";
  editingId = -1;
  testimonial = {};
  updateTextFields();
  if (show) M.toast({ html: "Cleard successfully", displayLength: 2400 });
  document.getElementById("add-btn").innerHTML =
    "<i class='material-icons left'>add</i>Add";
}

function updateTextFields() {
  setTimeout(() => {
    M.updateTextFields();
  }, 0);
}

function editTestimonial(index) {
  event.stopPropagation();
  customerName.focus();
  addCssClasses();
  document.getElementById("add-btn").innerHTML =
    "<i class='material-icons left'>update</i>Update";
  customerName.value = testimonialArray[index].customerName;
  content.value = testimonialArray[index].content;
  forPerson.value = testimonialArray[index].forPerson;
  editingId = testimonialArray[index].id;
  M.toast({ html: "Selected item can be editable now", displayLength: 2400 });
  updateTextFields();
  removeCssClasses();
}

function removeCssClasses() {
  setTimeout(() => {
    document.getElementById("edit-area").classList.remove("zoom");
    document.getElementById("edit-area").classList.add("zoom-out");
    var collapsibles = document.getElementsByClassName("collapsible-header");
    for (let loop = 0; loop < collapsibles.length; loop++) {
      collapsibles[loop].classList.remove("bg-border-light-black");
    }
    document
      .getElementById("view-area")
      .classList.remove("bg-border-light-black");
    document.body.style.backgroundColor = "#f2f2f2";
  }, 1300);
}

function addCssClasses() {
  document.getElementById("edit-area").classList.add("zoom");
  var collapsibles = document.getElementsByClassName("collapsible-header");
  for (let loop = 0; loop < collapsibles.length; loop++) {
    collapsibles[loop].classList.add("bg-border-light-black");
  }
  document.getElementById("view-area").classList.add("bg-border-light-black");
  document.body.style.backgroundColor = "#ccc";
}

function removeTestimonial(index, event) {
  event.stopPropagation();
  testimonialArray.splice(index, 1);
  if (editingId == index) this.clearTestimonial(false);
  if (testimonialArray.length == 0) totalId = 0;
  M.toast({ html: "Testimonial deleted successfully", displayLength: 2400 });
  showTestimonials();
}

function showTestimonials() {
  if (testimonialArray.length == 0) {
    msgForTestimonial();
    return;
  }
  document.getElementById("msgTestimonicalDiv").style.display = "none";
  var testimonialDiv = "<ul class='collapsible'>";
  for (let loop = 0; loop < testimonialArray.length; loop++) {
    testimonialDiv += "<li>";
    testimonialDiv +=
      "<div class='collapsible-header'><span class='font-500'>" +
      testimonialArray[loop].customerName +
      "</span><div style='margin-left:auto'><i class='material-icons other-icon' onclick ='editTestimonial(" +
      loop +
      ")'>edit</i><i class='material-icons other-icon'  onclick ='removeTestimonial(" +
      loop +
      ",event)'>delete</i></div></div>";
    testimonialDiv +=
      "<div class='collapsible-body'><span> " +
      testimonialArray[loop].content +
      "<div style='margin-top:10px'><span class='font-500'>For:</span> " +
      testimonialArray[loop].forPerson +
      "</span></div>";
    testimonialDiv += "</li>";
  }
  testimonialDiv += "</ul>";
  document.getElementById("testimonialDiv").innerHTML = testimonialDiv;
  setTimeout(() => {
    initializeCollapsible();
  }, 1000);
}

function msgForTestimonial() {
  document.getElementById("testimonialDiv").innerHTML = "";
  document.getElementById("msgTestimonicalDiv").style.display = "block";
}

function submit() {
  if (this.testimonialArray.length == 0) {
    M.toast({ html: "Kindly add atleast one tesimonial", displayLength: 2400 });
  } else {
    document.getElementById("open-modal").click();
    var obj = {
      testimonials: testimonialArray
    };
    var fileContents = JSON.stringify(obj, null, 4);
    const blob = new Blob([fileContents], { type: "application/json" });
    const file = new File([blob], "testimonials.json");
    uploadToS3(file);
  }
}

function uploadToS3(file) {
  var BUCKET_NAME = "fitestimonials";
  let s3bucket = new AWS.S3({
    accessKeyId: "AKIA4AWK7OODN6WZHNUM",
    secretAccessKey: "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",
    region: "ap-south-1",
    Bucket: BUCKET_NAME
  });
  var params = {
    Bucket: BUCKET_NAME,
    Key: file.name,
    Body: file
  };
  s3bucket.upload(params, function(err, data) {
    document.getElementById("close-modal").click();
    if (err) {
      M.toast({
        html: "There is some issue in submit process",
        displayLength: 2400
      });
      console.log("error in callback");
      console.log(err);
      return;
    }
    resetAll();
    M.toast({ html: "Submitted successfully", displayLength: 2400 });
  });
}

function resetAll() {
  this.totalId = 0;
  testimonialArray = [];
  clearTestimonial(false);
  showTestimonials();
}
